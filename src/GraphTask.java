import java.util.*;
import java.util.stream.Collectors;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 *
 * @author Hugo Rene Udumäe.
 * @author Jarmo Jevonen.
 * @author Kaur-Kristofer Suik.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     * Some basic examples are included but commented out.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.println(g.toString());
        System.out.println(g.getCenterVertices());

        Graph x = new Graph("X");
        x.createRandomSimpleGraph(12, 18);
        System.out.println(x.toString());
        System.out.println(x.getCenterVertices());

        Graph v = new Graph("V");
        v.createRandomSimpleGraph(34, 56);
        System.out.println(v.toString());
        System.out.println(v.getCenterVertices());

//        Graph z = new Graph("Z");
//        z.createRandomSimpleGraph(100, 156);
//        System.out.println(z.toString());
//        System.out.println(z.getCenterVertices());

//        long startTime = System.nanoTime();
//        Graph y = new Graph("Y");
//        y.createRandomSimpleGraph(2000, 3000);
//        System.out.println(y.toString());
//        System.out.println(y.getCenterVertices());
//        long endTime = System.nanoTime();
//        System.out.println("Execution in milliseconds: " + (endTime - startTime) / 1000000);

        //Kaur-Kristofer Suik main examples
        Graph h = new Graph("H");
        h.createRandomSimpleGraph(8, 9);
        System.out.println(h);
        ArrayList<ArrayList<Arc>> arcsOfVertexies = h.findVerticesThatAreInCycle(h.first);
        System.out.println(arcsOfVertexies.toString());

        //Combined methods example
        Graph j = new Graph("J");
        j.createRandomSimpleGraph(40, 70);
        System.out.println(j);
        List<Vertex> centers = j.getCenterVertices();
        System.out.println("The center points: " + centers);
        System.out.println("List of cycle vertices: " + j.findVerticesThatAreInCycle(j.first));
        for (Vertex centerPoint : centers) {
            System.out.println("Farthest path from: " + centerPoint + " is: " + j.findFarthestVertexFromGivenVertex(centerPoint));
        }
    }

    /**
     * Vertex class represents a single point in a graph.
     */
    class Vertex {
        private final String id;
        public Vertex next;
        public Arc first;
        private int info = 0;
        private Integer colorOfVertex = 0; // 0 = valge, 1 = hall, 2 = must

        /**
         * Class constructor with specified id, next Vertex and first Arc.
         *
         * @param s String input that is assigned to Vertex class id.
         * @param v Vertex assigned to next value.
         * @param e Arc assigned to value first.
         */
        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        /**
         * Class constructor for use with only specified id.
         *
         * @param s String input that is assigned to Vertex class id.
         */
        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Method compares two Vertices id's. If id's are equal, returns true. If not, returns false.
         *
         * @param v vertix to compare
         */
        public boolean equals(Vertex v) {
            return this.id.equals(v.id);
        }

        /**
         * @return - Adjacent vertices for this vertex
         * @author Jarmo Jevonen
         * Find adjacent vertices for this vertex
         */
        private ArrayList<Vertex> findAdjVertices() {
            ArrayList<Vertex> adjVertices = new ArrayList<>();
            if (first == null) return new ArrayList<>();
            adjVertices.add(first.target);
            Arc arc = first.next;
            if (arc != null) {
                while (arc.next != null) {
                    adjVertices.add(arc.target);
                    arc = arc.next;
                }
                adjVertices.add(arc.target);
            }
            return adjVertices;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private final String id;
        public Vertex target;
        public Arc next;

        /**
         * Class constructor that adds Arc id, target Vertex of Arc and next Arc.
         *
         * @param s String value assigned to id.
         * @param v Vertex class assigned to target.
         * @param a Arc class assigned to next.
         */
        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Class constructor when only id is given.
         * Everything else is given default value of null.
         *
         * @param s String value assigned to id.
         */
        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    /**
     * Graph represents a collection of vertices that are connected using Arcs.
     * The graph class is setup to hold the first Vertex of a graph that contains references to the following vertices and Arcs.
     */
    class Graph {

        private final String id;
        public Vertex first;
        private int info = 0;
        private int vertexCount = 0;
        private final ArrayList<Vertex> greyVertexArray = new ArrayList<>();
        private final ArrayList<ArrayList<Vertex>> cycleArray = new ArrayList<>();

        /**
         * Graph constructor when graph Name and first Vertex are provided.
         *
         * @param s String value assigned to id.
         * @param v Vertex assigned to value first.
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Method getVertexCount is used to return the private value this.vertexCount in class Graph.
         * Used by several methods to parse amount vertices for matrix creation
         *
         * @return this.vertexCount Integer count of the total amount of vertices in Graph.
         */
        public int getVertexCount() {
            return this.vertexCount;
        }


        /**
         * Graph class method that creates a new Vertex and implements it into the Graph class instance.
         * This method also increases the Graph class private variable vertexCount by +1 every time a Vertex is added.
         * This is used by several other methods, such as getCenterVertices, to create matrices.
         *
         * @param vid the name of the vertex
         * @return returns the newly created Vertex class
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            this.vertexCount++; //Increases vertexCount by 1.
            return res;
        }


        /**
         * Graph class method that creates an Arc between a start Vertex and a destination Vertex.
         * In order to create an edge, 2 arcs need to be created with the 'from' and 'to' variables reversed.
         *
         * @param aid  the name of the arc
         * @param from the vertex where the arc is starting from
         * @param to   the destination of the arc
         * @return returns the created Arc class
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Graph class method, that uses DFS to find vertices, that are in one or more cycles.
         * The method remembers in which cycle vertex was found. Afterwards, the method converts ArrayList(Vertex) into a
         * ArrayList(Arc) to get better visualisation of the cycles, where the vertices are coming from.
         *
         * @param v first vertex ( place where DFS starts)
         * @return returns a ArrayList of ArrayLists(represent cycles) of Arcs. When this list is empty, that means there are no cycles.
         */
        public ArrayList<ArrayList<Arc>> findVerticesThatAreInCycle(Vertex v) {

            deptFirstSearch(v, null); // First vertex in cycle does not have an older vertex.

            ArrayList<ArrayList<Arc>> listOfArcList = new ArrayList<>();

            if (cycleArray.size() < 1) {
                return listOfArcList;
            }

            for (ArrayList<Vertex> array : cycleArray) { // to make ArrayList<Vertex> --> ArrayList<Arc>
                ArrayList<Arc> arcList = new ArrayList<>();

                for (int j = 0; j < array.size(); j++) {

                    Arc a = array.get(j).first;
                    while (a != null) {
                        if (j + 1 < array.size()) {
                            if (a.target.equals(array.get(j + 1))) {
                                arcList.add(a);
                                break;
                            }
                        } else if (a.target.equals(array.get(0))) { //to get the last arc
                            arcList.add(a);
                            break;
                        }
                        a = a.next;
                    }
                }
                listOfArcList.add(arcList);
            }

            //System.out.println(cycleArray.toString());
            return listOfArcList;
        }

        /**
         * Method implements DFS to a graph, Goes through all the vertices and tries to find cycles.
         * When cycle is found, method starts a new method to count vertices in that cycle.
         *
         * @param v            first vertex
         * @param parentVertix parent vertex of current vertx(first vertx does not have a parent vertex)
         */
        public void deptFirstSearch(Vertex v, Vertex parentVertix) {

            if (v.colorOfVertex == 2) { //completely traversed vertex
                return;
            }

            if (v.colorOfVertex == 1) { //a almost completely traversed vertex.
                findCycle(v);
                return;
            }

            if (!greyVertexArray.contains(v)) {
                v.colorOfVertex = 1; //
                greyVertexArray.add(v);

                Arc a = v.first;
                while (a != null) {
                    if (a.target != parentVertix) {
                        deptFirstSearch(a.target, v);
                    }
                    a = a.next;
                }
            }

            v.colorOfVertex = 2;
            greyVertexArray.remove(v);
        }


        /**
         * This method starts, when a cycle has been spotted in the graph. It creates a new ArrayList to
         * hold the Vertices of this cycle. It goes through greyVertexArray, where all not fully completed
         * vertices are held. It searches for the starting point of the cycle until it finds one. If it found a cycle then all vertices that come
         * after starting point are in the cycle. All following vertices are added into an ArrayList that contains the Arcs of that cycle.
         *
         * @param v first vertex in a cycle
         */
        private void findCycle(Vertex v) {
            ArrayList<Vertex> newCycle = new ArrayList<>();
            boolean CycleCounter = false; //Signals the start of the loop.

            for (Vertex vertex : greyVertexArray) {
                if (vertex.equals(v)) {
                    CycleCounter = true;
                }
                if (CycleCounter) {
                    newCycle.add(vertex);
                }
            }
            cycleArray.add(newCycle);
        }

        /**
         * Main solution method getCenterVertices(). Used to obtain a list of all center vertices.
         * Calls the getCenterVertexIndexes private method to retrieve a list of all center Vertex indexes.
         * Uses custom int value depthCount to keep track of what vertex depth the loop is currently on.
         * These indexes are compared to the depthCount custom size to parse if current Vertex in while loop is a center Vertex or not.
         *
         * @return centerVertices A list of center vertices.
         */
        public List<Vertex> getCenterVertices() {
            List<Integer> centerVertexIndexes = getCenterVerticesIndexes();
            List<Vertex> centerVertices = new ArrayList<>();

            info = 0;
            Vertex v = first;
            int depthCount = 0;
            while (v != null) {
                if (centerVertexIndexes.contains(depthCount)) {
                    centerVertices.add(v);
                }
                v.info = info++;
                v = v.next;
                depthCount++;
            }

            return centerVertices;
        }

        /**
         * @param startingVertex - From which vertex yo find farthest vertices from
         * @return - Vertices farthest from the startingVertex
         * @author Jarmo Jevonen
         * Find farthest vertices from given starting vertex. Graph vertices must be connected with two directional
         * connections. Graphs all points have to be accessible from any given point.
         */
        public ArrayList<Vertex> findFarthestVertexFromGivenVertex(Vertex startingVertex) {
            ArrayList<Vertex> usedVertices = new ArrayList<>();
            usedVertices.add(startingVertex);
            ArrayList<Vertex> previousTier;
            ArrayList<Vertex> highestTier = startingVertex.findAdjVertices();
            if (highestTier.isEmpty()) return highestTier;
            do {
                previousTier = highestTier;
                highestTier = new ArrayList<>();
                usedVertices.addAll(previousTier);
                for (Vertex vertex : previousTier) {
                    ArrayList<Vertex> vertices = vertex.findAdjVertices().stream().filter(vertex1 -> !usedVertices.contains(vertex1)).collect(Collectors.toCollection(ArrayList::new));
                    highestTier.addAll(vertices);
                }
                highestTier = highestTier.stream().distinct().collect(Collectors.toCollection(ArrayList::new));
            } while (highestTier.size() != 0);
            return previousTier;
        }

        /**
         * Creates a distance matrix from adjacency matrix.
         * Uses the method getVertexCount from Graph class to parse number of total vertices.
         * Each edge is marked with the value 1, if two vertices are not adjacent they are given a placeholder INFINITY value.
         * INFINITY value is generated by algorithm = 2 * vertexCount + 1
         * Same Vertex adjacency is marked as 0, f.e v1 and v1.
         *
         * @param mat Adjacency matrix, usually generated by method createAdjMatrix() in class Graph
         * @return matrix of distances, where each edge has length 1
         * <p>
         * Code taken from the course materials and modified for purposes of this homework.
         * Source URL: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
         */
        public int[][] createDistMatrix(int[][] mat) {
            int nvert = getVertexCount();
            int[][] result = new int[nvert][nvert];
            if (nvert < 1) return result;
            int INFINITY = 2 * nvert + 1; // Integer.MAX_VALUE / 2; // NB!!!
            for (int i = 0; i < nvert; i++) {
                for (int j = 0; j < nvert; j++) {
                    if (mat[i][j] == 0) {
                        result[i][j] = INFINITY;
                    } else {
                        result[i][j] = 1;
                    }
                }
            }
            for (int i = 0; i < nvert; i++) {
                result[i][i] = 0;
            }

            return result;
        }

        /**
         * Calculates shortest paths using Floyd-Warshall algorithm.
         * This matrix will contain shortest paths between each pair of vertices.
         * Repurposes the existing distanceMatrix into a matrix that contains shortest Paths between all vertices.
         * The value 0 represents a same Vertex arc, such as (V1, V1);
         *
         * @param matrix A matrix consisting of int values. This matrix is expected to be a distanceMatrix using the method createDistMatrix.
         * @return matrix An updated matrix that contains updated shortest paths between all vertices.
         * <p>
         * Code taken from the course materials and modified for purposes of this homework.
         * Source URL: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
         */
        public int[][] shortestPaths(int[][] matrix) {
            int n = getVertexCount(); // number of vertices
            if (n < 1) return matrix;
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        int newLength = matrix[i][k] + matrix[k][j];
                        if (matrix[i][j] > newLength) {
                            matrix[i][j] = newLength; // new path is shorter
                        }
                    }
                }
            }
            return matrix;
        }

        /**
         * Private method printTable is solely used to return a more read-able Matrix of int[][] in a string format.
         * This method allows a user to more easily parse what is contained in a matrix and potentially debug faster.
         * The string is built using a StringBuilder class.
         *
         * @param table A matrix of int[][]. This is the matrix that is read and.
         * @return answer The string parsed from inputted matrix table.
         */
        private String printTable(int[][] table) {
            StringBuilder answer = new StringBuilder();
            answer.append("| ");

            for (int i = 0; i < info; i++) {
                for (int j = 0; j < info; j++) {
                    answer.append(table[i][j]).append(" | ");
                }
                answer.append("\n| ");
            }
            answer.append("\b\b");
            return answer.toString();
        }

        /**
         * A private method that is used solely by the method getCenterVertices to get a list of all the indexes of center vertices.
         * getCenterVertexIndexes obtains a matrix of int[][] filled with the shortest paths between Vertices. It calls the shortestPaths method for this.
         * getCenterVertexIndexes adds together all the rows in the matrix generated by method shortestPath and then picks out the smallest values out of all the rows.
         *
         * @return centerVertexIndexes A List<Integer> of center vertex indexes.
         */
        private List<Integer> getCenterVerticesIndexes() {
            int[][] matrix = shortestPaths(createDistMatrix(createAdjMatrix()));
            Map<Integer, Integer> vertexPaths = new HashMap<>();
            for (int i = 0; i < getVertexCount(); i++) {
                int tempSum = 0;
                for (int j = 0; j < getVertexCount(); j++) {
                    tempSum += matrix[i][j];
                }
                vertexPaths.put(i, tempSum);
            }

            List<Integer> centerVertexIndexes = new ArrayList<>();

            int minimalTotalPaths = Collections.min(vertexPaths.values());

            for (Map.Entry<Integer, Integer> entry : vertexPaths.entrySet()) {
                if (entry.getValue().equals(minimalTotalPaths)) {
                    centerVertexIndexes.add(entry.getKey());
                }
            }

            return centerVertexIndexes;
        }
    }
}